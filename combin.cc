#include <cstddef>
#include <type_traits>
#include <memory>

template <typename C, typename V>
struct BoundComb;
template <typename C1, typename C2>
struct ConcatComb;
template <typename C, size_t count>
struct RepeatStaticComb;
template <typename C>
struct RepeatComb;

typedef char **Stream;

template <typename Comb>
struct BaseComb {
    template <typename V>
    constexpr BoundComb<Comb, V> bind(V &v) const;

    template <typename C>
    constexpr ConcatComb<Comb, C> concat(C c) const;
    template <typename C>
    constexpr ConcatComb<Comb, C> operator + (C c) const {
        return concat(c);
    }

    template <size_t count>
    constexpr RepeatStaticComb<Comb, count> repeat_static() const;

    constexpr RepeatComb<Comb> repeat(size_t count) const;
    constexpr RepeatComb<Comb> operator * (size_t count) const {
        return repeat(count);
    }
};

struct EmptyComb : public BaseComb<EmptyComb> {
    void exec(EmptyComb, Stream, ...) {
    }
    template <typename Comb> // is it valid?
    void exec(const Comb, Stream, ...) const {
        static_assert(std::is_same<Comb, EmptyComb>::value, "invalid EmptyCombe exec");
    }
};

template <typename C, typename V>
struct BoundComb : public BaseComb<BoundComb<C, V>> {
    const C c;
    V &v;
    BoundComb(C c, V &v): c(c), v(v) {}
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, Args &&(...args)) const {
        return c.exec(next, in, v, std::forward<Args>(args)...);
    }
};

template <typename C1, typename C2>
struct ConcatComb : public BaseComb<ConcatComb<C1, C2>> {
    const C1 c1;
    const C2 c2;
    ConcatComb(C1 c1, C2 c2): c1(c1), c2(c2) {}
    template <typename ...Args>
    void exec(EmptyComb, Stream in, Args &&(...args)) const {
        return c1.exec(c2, in, std::forward<Args>(args)...);
    }
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, Args &&(...args)) const {
        return c1.exec(c2 + next, in, std::forward<Args>(args)...);
    }
};
template <typename C>
struct ConcatComb<C, EmptyComb> : public BaseComb<ConcatComb<EmptyComb, C>> {
    const C c;
    ConcatComb(C c, EmptyComb): c(c) {}
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, Args &&(...args)) const {
        return c.exec(next, in, std::forward<Args>(args)...);
    }
};
template <typename C>
struct ConcatComb<EmptyComb, C> : public BaseComb<ConcatComb<EmptyComb, C>> {
    const C c;
    ConcatComb(EmptyComb, C c): c(c) {}
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, Args &&(...args)) const {
        return c.exec(next, in, std::forward<Args>(args)...);
    }
};
template <typename Comb>
template <typename C>
constexpr ConcatComb<Comb, C> BaseComb<Comb>::concat(C c) const {
    return ConcatComb<Comb, C>(*static_cast<const Comb *>(this), c);
}

template <typename C, size_t count>
struct RepeatStaticComb : public BaseComb<RepeatStaticComb<C, count>> {
    const C c;
    RepeatStaticComb(C c): c(c) {}
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, Args &&(...args)) const {
        return c.exec(c.template repeat_static<count - 1>() + next, in, std::forward<Args>(args)...);
    }
};
template <typename C>
struct RepeatStaticComb<C, 0> : public BaseComb<RepeatStaticComb<C, 0>> {
    RepeatStaticComb(C) {}
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, Args &&(...args)) const {
        return next.exec(EmptyComb(), in, std::forward<Args>(args)...);
    }
};
template <typename Comb>
template <size_t count>
constexpr RepeatStaticComb<Comb, count> BaseComb<Comb>::repeat_static() const {
    return RepeatStaticComb<Comb, count>(*static_cast<const Comb *>(this));
}

struct RepListBase {};
template <typename ...TS>
struct RepList : public RepListBase {};
template <typename T, typename ...TS>
struct RepList<T, TS...> : public RepListBase {
    T h;
    std::unique_ptr<RepList<TS...>> r;
    RepList(T &&lv, TS &&(...ts)): h(lv), r(std::make_unique<RepList<TS...>>(ts...)) {}
};
template <>
struct RepList<> : public RepListBase {
};
template <typename ...TS>
RepList<TS...> _(TS &&(...ts)) {
    return RepList<TS...>{std::forward<TS>(ts)...};
}

template <typename C>
struct RepeatComb : public BaseComb<RepeatComb<C>> {
    const C c;
    const size_t count;
    RepeatComb(C c, size_t count): c(c), count(count) {}

    struct RepeatCombInternal : public BaseComb<RepeatCombInternal> {
        const C c;
        size_t count;
        RepeatCombInternal(C c, size_t count): c(c), count(count) {}
        template <typename Comb, typename T, typename ...Args>
        void exec(Comb next, Stream in, T v, Args &&(...args)) const {
            if (count == 0) {
                next.exec(EmptyComb(), in, std::forward<Args>(args)...);
            } else {
                auto &x = *v;
                ++v;
                c.exec(RepeatCombInternal{c, count-1} + next, in, x, v, std::forward<Args>(args)...);
            }
        }
    };
    template <typename Comb, typename T, size_t N, typename ...Args>
    void exec(Comb next, Stream in, T (&v)[N], Args &&(...args)) const {
        RepeatCombInternal(c, count).exec(next, in, static_cast<T *>(v), std::forward<Args>(args)...);
    }

    template <typename Comb, typename RepT, typename ...Args>
    void exec(Comb next, Stream in, RepT &&v, Args &&(...args),
        std::enable_if_t<
            std::is_base_of<RepListBase, std::remove_reference_t<RepT>>::value &&
            !std::is_same<RepList<>, std::remove_reference_t<RepT>>::value,
            nullptr_t
        > = nullptr
    ) const {
        if (count == 0) {
            next.exec(EmptyComb(), in, std::forward<Args>(args)...);
        } else {
            return c.exec(RepeatComb{c, count-1} + next, in, v.h, *v.r, std::forward<Args>(args)...);
        }
    }
    template <typename Comb, typename RepT, typename ...Args>
    void exec(Comb next, Stream in, RepT &&, Args &&(...args),
        std::enable_if_t<
            std::is_same<RepList<>, std::remove_reference_t<RepT>>::value,
            nullptr_t
        > = nullptr
    ) const {
        return next.exec(EmptyComb(), in, std::forward<Args>(args)...);
    }
};
template <typename Comb>
constexpr RepeatComb<Comb> BaseComb<Comb>::repeat(size_t count) const {
    return RepeatComb<Comb>{*static_cast<const Comb *>(this), count};
}


struct CharComb : public BaseComb<CharComb> {
    template <typename Comb, typename ...Args>
    void exec(Comb next, Stream in, char &ch, Args &&(...args)) const {
        ch = **in;
        (*in)++;
        return next.exec(EmptyComb(), in, std::forward<Args>(args)...);
    }
};


#include <cstdio>
char src[] = "hello, world!", *in = src;
CharComb C;

int main() {
    char chs[16];
    (C * 3).exec(EmptyComb(), &in, _(chs[0], chs[2], chs[1]));
    char (*pchs)[13];
    pchs = (char (*)[13])(chs+3);
    (C * 11).exec(EmptyComb(), &in, *pchs);
    printf("%s\n", chs);
    return 0;
}

